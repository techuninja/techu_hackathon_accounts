package com.techu.hackathon.account.service;

import com.techu.hackathon.account.dto.AccountDto;
import com.techu.hackathon.account.dto.BalanceDto;
import com.techu.hackathon.account.model.AccountModel;
import com.techu.hackathon.account.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {

    AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository)
    {
        this.accountRepository = accountRepository;
    }


    @Override
    public List<AccountModel> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public List<AccountModel> findByCliente(String codCliente) {
        return accountRepository.findByCliente(codCliente);
    }

    @Override
    public AccountModel nuevaCuenta(AccountModel cuenta) {
        return accountRepository.nuevaCuenta(cuenta);
    }

    @Override
    public void eliminarCuenta(String nroCuenta) {
        accountRepository.eliminarCuenta(nroCuenta);
    }

    @Override
    public AccountModel findByNroCuenta(String nroCuenta) {
        return accountRepository.findByNroCuenta(nroCuenta);
    }

}
