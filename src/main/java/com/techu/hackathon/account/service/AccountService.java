package com.techu.hackathon.account.service;

import com.techu.hackathon.account.dto.AccountDto;
import com.techu.hackathon.account.dto.BalanceDto;
import com.techu.hackathon.account.model.AccountModel;

import java.util.List;

public interface AccountService {

    List<AccountModel> findAll();
    List<AccountModel> findByCliente(String codCliente);

    AccountModel nuevaCuenta(AccountModel cuenta);
    void eliminarCuenta(String nroCuenta);

    AccountModel findByNroCuenta(String nroCuenta);
}
