package com.techu.hackathon.account.controller;

import com.techu.hackathon.account.dto.AccountDto;
import com.techu.hackathon.account.dto.BalanceDto;
import com.techu.hackathon.account.model.AccountModel;
import com.techu.hackathon.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping(value = "/{cliente}")
    public ResponseEntity<List<AccountModel>> obtenerCuentas(@PathVariable("cliente") String cliente) {
        System.out.println("Me piden la lista de cuentas");
        return ResponseEntity.ok(accountService.findByCliente(cliente));
    }


    @PostMapping()
    public ResponseEntity<AccountModel> nuevaCuenta(@RequestBody AccountModel cuenta)
    {
        return ResponseEntity.ok(accountService.nuevaCuenta(cuenta));
    }

    @DeleteMapping(value = "/{nroCuenta}")
    public ResponseEntity<Object> eliminarCuenta(@PathVariable("nroCuenta") String nroCuenta){
        if(accountService.findByNroCuenta(nroCuenta)!=null){
            accountService.eliminarCuenta(nroCuenta);
            return new ResponseEntity<>(String.format("Cuenta %s eliminada", nroCuenta), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(String.format("Cuenta %s no existe",nroCuenta),HttpStatus.NOT_FOUND);
        }
    }

//    @PatchMapping(value = "/{id}/charge", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Object> balanceToCharge(@PathVariable("id") String id, @RequestBody BalanceDto balance){
//        AccountDto dto = service.balanceToCharge(id, balance);
//        /*if(id.equals("12345678912345")){
//            return new ResponseEntity<>("Error", HttpStatus.BAD_REQUEST);
//        }*/
//        return new ResponseEntity<>(dto, HttpStatus.CREATED);
//    }
//
//    @PatchMapping(value = "/{id}/credit", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Object> balanceToCredit(@PathVariable("id") String id, @RequestBody BalanceDto balance){
//        AccountDto dto = service.balanceToCredit(id, balance);
//        return new ResponseEntity<>(dto, HttpStatus.CREATED);
//    }
}
