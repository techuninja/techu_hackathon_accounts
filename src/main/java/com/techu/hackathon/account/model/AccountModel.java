package com.techu.hackathon.account.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "accounts")
public class AccountModel {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String nroCuenta;
    private ProductModel producto;
    private String codigoCliente;
    private double saldoDisponible;
    private double saldoConsumido;
    private String moneda;
    private Boolean activo;



    public AccountModel() {
    }

    public AccountModel(@NotNull String id, String nroCuenta,ProductModel producto,String codigoCliente,
            double saldoDisponible,double saldoConsumido,String moneda,Boolean activo) {

        this.setId(id);
        this.setNroCuenta(nroCuenta);
        this.setProducto(producto);
        this.setCodigoCliente(codigoCliente);
        this.setSaldoDisponible(saldoDisponible);
        this.setSaldoConsumido(saldoConsumido);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public ProductModel getProducto() {
        return producto;
    }

    public void setProducto(ProductModel producto) {
        this.producto = producto;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public double getSaldoDisponible() {
        return saldoDisponible;
    }

    public void setSaldoDisponible(double saldoDisponible) {
        this.saldoDisponible = saldoDisponible;
    }

    public double getSaldoConsumido() {
        return saldoConsumido;
    }

    public void setSaldoConsumido(double saldoConsumido) {
        this.saldoConsumido = saldoConsumido;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}
