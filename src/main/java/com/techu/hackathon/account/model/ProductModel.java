package com.techu.hackathon.account.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class ProductModel {

    @NotNull
    private String tipo;
    @NotNull
    private String nombre;

    public ProductModel(String tipo, String nombre){
        this.setTipo(tipo);
        this.setNombre(nombre);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
