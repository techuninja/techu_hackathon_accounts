package com.techu.hackathon.account.dto;

public class AccountDto {

    private String accountId;

    public AccountDto(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
