package com.techu.hackathon.account.repository;

import com.techu.hackathon.account.model.AccountModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountRepositoryImpl implements AccountRepository{
    private final MongoOperations mongoOperations;

    @Autowired
    public AccountRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<AccountModel> findAll() {
        List<AccountModel> cuentas = this.mongoOperations.find(new Query(), AccountModel.class);
        System.out.println("cuentas:"+cuentas.toString());
        return cuentas;
    }

    @Override
    public AccountModel findByNroCuenta(String nroCuenta) {
        AccountModel encontrado = this.mongoOperations.findOne(new Query(Criteria.where("nroCuenta").is(nroCuenta)),AccountModel.class);
        return encontrado;
    }

    @Override
    public List<AccountModel> findByCliente(String codCliente) {
        List<AccountModel> cuentas = this.mongoOperations.find(new Query(Criteria.where("codigoCliente").is(codCliente)),AccountModel.class);
        return cuentas;
    }

    @Override
    public AccountModel nuevaCuenta(AccountModel cuenta) {
        this.mongoOperations.save(cuenta);
        return findByNroCuenta(cuenta.getNroCuenta());
    }

    @Override
    public void eliminarCuenta(String nroCuenta) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("nroCuenta").is(nroCuenta)), AccountModel.class);
    }

}
