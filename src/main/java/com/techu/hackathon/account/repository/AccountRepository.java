package com.techu.hackathon.account.repository;

import com.techu.hackathon.account.model.AccountModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository {

    List<AccountModel> findAll();
    List<AccountModel> findByCliente(String codCliente);
    AccountModel findByNroCuenta(String nroCuenta);
    AccountModel nuevaCuenta(AccountModel cuenta);
    void eliminarCuenta(String nroCuenta);
}
