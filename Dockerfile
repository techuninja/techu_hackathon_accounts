FROM openjdk:11-jre-slim-buster
EXPOSE 8081
ARG JAR_FILE=target/account-1.0.0.jar
ADD ${JAR_FILE} account.jar
ENTRYPOINT ["java","-jar","/account.jar"]